# MkDocs Builder for HEIA-FR
The CI/CD of this repo builds and publish a Docker image for building MkDocs web sites.

## Description

This docker image is based on [squidfunk/mkdocs-material](https://hub.docker.com/r/squidfunk/mkdocs-material) docker image and add the ```mkdocs-macros-plugin```

## Build locally to test 

To test if the image can be build without error, test locally on your machine with the command:
```
docker build -t mkdocs-edu .
```


## Force rebuild

If we change the content of the ```Dockerfile``` we have to add a new Tag to this repo. Then the CI/CD Pipeline will be executed.
Go to the "Repository--> Tag" menu to add the latest version number.

## How to use this image

To download and use this image, just type:
```
docker pull registry.forge.hefr.ch/bun/dockers/mkdocs-edu
```

Then you can use it to visualize your website made with MkDocs:
```
docker run --rm -it -p 8000:8000 -w $(pwd)/docs -v $(pwd):/home registry.forge.hefr.ch/francois.buntschu/mkdocs-edu serve -f /home/mkdocs.yml
```
Note: Just start this command in the root directory of your MkDocs website or change the path to the ```docs```directory.

## TODO
In the future release, I will have to add and customize the following plugins:
- https://timvink.github.io/mkdocs-print-site-plugin/index.html
- https://github.com/timvink/mkdocs-enumerate-headings-plugin
  

## More informations
See Gitlab Docs:
* https://docs.gitlab.com/ee/user/packages/container_registry/


<hr>
(c) F. Buntschu 30.06.2022